/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
type ReversedListNode struct {
    Prev *ReversedListNode
    Original *ListNode
}
func removeNthFromEnd(head *ListNode, n int) *ListNode {
    if head.Next == nil {
        if n == 1 {
            return nil
        }
        return head
    }
    tail := &ReversedListNode{
        Prev: nil,
        Original: head,
    }
    prev := &ReversedListNode{}
    for current := head.Next; current != nil; current = current.Next {
        prev = tail
        tail = &ReversedListNode{
            Prev: prev,
            Original: current,
        }
    }
    for i := 1; i < n; i++ {
        prev = tail
        tail = tail.Prev
    }
    if tail.Prev != nil {
        beforeSkip := tail.Prev.Original
        beforeSkip.Next = beforeSkip.Next.Next
    } else {
        head = head.Next
    }
    return head
}