/**
 * Definition for a binary tree node.
 * type TreeNode struct {
 *     Val int
 *     Left *TreeNode
 *     Right *TreeNode
 * }
 */
func isSameTree(p *TreeNode, q *TreeNode) bool {
    if p == nil || q == nil {
        if p == q {
            return true
        }
        return false
    }
    nextP := []*TreeNode{
        p,
    }
    nextQ := []*TreeNode{
        q,
    }
    for len(nextP) > 0 {
        p = nextP[0]
        nextP = nextP[1:]
        q = nextQ[0]
        nextQ = nextQ[1:]
        if p.Val != q.Val {
            return false
        }
        if p.Left != nil && q.Left != nil {
            nextP = append(nextP, p.Left)
            nextQ = append(nextQ, q.Left)
        } else if p.Left != q.Left {
            return false
        }
        if p.Right != nil && q.Right != nil {
            nextP = append(nextP, p.Right)
            nextQ = append(nextQ, q.Right)
        } else if p.Right != q.Right {
            return false
        }
    }
    if len(nextQ) > 0 {
        return false
    }
    return true
}