/**
 * Definition for singly-linked list.
 * type ListNode struct {
 *     Val int
 *     Next *ListNode
 * }
 */
func reverseBetween(head *ListNode, m int, n int) *ListNode {
    if head.Next == nil {
        return head
    }
    result := head
    stack := []*ListNode{}
    for i := 1; i < m - 1; i++ {
        head = head.Next
    }
    from := head
    if m > 1 {
        head = head.Next
    } 
    for i := 0; i <= n - m; i++ {
        stack = append([]*ListNode{head}, stack...)
        head = head.Next
    }
    idx := 0
    if m == 1 {
        from = stack[0]
        result = from
        idx = 1
    }
    to := head
    for i := idx; i < len(stack); i++ {
        from.Next = stack[i]
        from = from.Next
    }
    from.Next = to
    return result
}