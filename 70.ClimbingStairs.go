func climbStairs(n int) int {
    steps := []int{1, 1}
    for i := 2; i <= n; i++ {
        steps = append(steps, steps[i-1] + steps[i-2])
    }
    return steps[n]
}
/*
1 1
2 11, 2
3 111, 21, 12
4 1111, 22, 211, 112, 121
5 11111, 221, 122, 212, 1121, 1112, 1121, 1211
*/