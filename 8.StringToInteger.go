import "math"

func myAtoi(str string) int {
    result := 0
    digits := map[rune]int{
        '0':0,
        '1':1,
        '2':2,
        '3':3,
        '4':4,
        '5':5,
        '6':6,
        '7':7,
        '8':8,
        '9':9,
    }
    numberStarted := false
    isNegative := false
    isPositive := false
    for _, c := range str {
        _, exists := digits[c]
        if !exists && numberStarted {
            break
        }
        if c == ' ' {
            if numberStarted {
                break
            }
            if isNegative || isPositive {
                // no spaces between + or - and the number
                return 0
            }
            continue
        }
        if c == '-' {
            if isPositive || isNegative {
                // any two + and/or - are not allowed
                return 0
            }
            isNegative = true
            continue
        }
        if c == '+' {
            if isNegative || isPositive {
                // any two + and/or - are not allowed
                return 0
            }
            isPositive = true
            continue
        }
        if !exists && !numberStarted {
            return 0
        }
        numberStarted = true
        result = result * 10 + digits[c]
        
        if result > math.MaxInt32 {
            if isNegative {
                result = math.MinInt32
                return result
            }
            result = math.MaxInt32  
            return result
        } 
    }
    if isNegative {
        result *= -1
    }
    return result
}