func rotate(matrix [][]int)  {
    size := len(matrix)
    for i := 0; i < size / 2; i++ { 
        for j := i; j < size - i - 1; j++ {
            temp := matrix[i][j]
            matrix[i][j] = matrix[size - j - 1][i]
            matrix[size - j - 1][i] = matrix[size - i - 1][size - j - 1]
            matrix[size - i - 1][size - j - 1] = matrix[j][size - i - 1]
            matrix[j][size - i - 1] = temp
        }
    }
}