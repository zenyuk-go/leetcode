func maxArea(heights []int) int {
    result := 0
    left := 0
    right := len(heights) -1
    for left < right {
        leftLower := true
        height := heights[left]
        if heights[right] < heights[left] {
            leftLower = false
            height = heights[right]
        }
        area := height * (right - left)
        if area > result {
            result = area
        }
        if leftLower {
            left++
        } else {
            right--
        }
    }
    return result
}