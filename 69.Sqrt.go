func mySqrt(x int) int {
    if x < 2 {
        return x
    }
    l := 0
    r := x
    m := 0
    for l < r {
        m = l + (r - l)/2
        if m*m == x {
            return m
        } else if m*m > x {
            r = m
        } else {
            if l == m {
                return m
            }
            l = m
        }
    }
    return m
}