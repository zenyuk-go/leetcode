func sortColors(nums []int)  {
    blueCount := 0
    result := []int{}
    for _, color := range nums {
        switch color {
        case 0: result = append([]int{color}, result...)
        case 1: result = append(result, color)
        case 2: blueCount++
        }
    }
    for i := 0; i < blueCount; i++ {
        result = append(result, 2)
    }
    for i := 0; i < len(nums); i++ {
        nums[i] = result[i]
    }
}