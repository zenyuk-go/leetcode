func longestCommonPrefix(strs []string) string {
    length := len(strs)
    if length == 0 {
        return ""
    }
    if length == 1 {
        return strs[0]
    }
    lowestIdx := len(strs[0])
    for i := 1; i < length; i++ {
        if len(strs[i]) < lowestIdx {
            lowestIdx = len(strs[i])
        }
        for j := 0; (j < len(strs[i]) && j < len(strs[i-1]) && j < lowestIdx); j++ {
            if strs[i-1][j] != strs[i][j] {
                if j < lowestIdx {
                    lowestIdx = j
                }
            }
        }
    }
    return strs[0][:lowestIdx]
}