package problem10

func isMatch(s string, p string) bool {
	return checkVariant([]rune(s), 0, NewPatternString(p), 0)
}

func checkVariant(fullString []rune, sIdx int, pattern PatternString, pIdx int) bool {
	if sIdx >= len(fullString) && pIdx >= len(pattern) {
		// successfully completed
		return true
	}
	if sIdx >= len(fullString) && pIdx < len(pattern) {
		p := pattern[pIdx]
		if p.repeated {
			// string finished, but m.b. repetitive pattern left
			return checkVariant(fullString, sIdx, pattern, pIdx+1)
		} else {
			return false
		}
	}
	if sIdx < len(fullString) && pIdx >= len(pattern) {
		p := pattern[len(pattern)-1]
		if p.repeated && fullString[sIdx] == p.char {
			// pattern finished, but m.b. the rest of the string is repetitive trailing char
			return checkVariant(fullString, sIdx+1, pattern, pIdx)
		}
		return false
	}

	p := pattern[pIdx]
	s := fullString[sIdx]
	if !p.repeated {
		if p.anyChar || p.char == s {
			return checkVariant(fullString, sIdx+1, pattern, pIdx+1)
		} else {
			return false
		}
	} else {
		if p.anyChar || p.char == s {
			// 2 variants: empty char or char with value of "s" (or any for dot (.)
			return checkVariant(fullString, sIdx+1, pattern, pIdx) || checkVariant(fullString, sIdx, pattern, pIdx+1)
		} else {
			// doesn't match - skip a symbol in the pattern
			return checkVariant(fullString, sIdx, pattern, pIdx+1)
		}
	}
}
