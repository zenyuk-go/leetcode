package problem10

import (
	"fmt"
)

type RepeatedChar struct {
	anyChar  bool
	char     rune
	repeated bool
}

func (r RepeatedChar) String() string {
	if r.repeated {
		return fmt.Sprintf("char: %s repeated\n", string(r.char))
	}
	return fmt.Sprintf("char: %s\n", string(r.char))
}
