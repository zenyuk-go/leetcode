
func reverse(x int) int {
input := int32(x)
    isNegative := input < 0
    result := int32(0)
    
    if isNegative {
        input *= -1
    }
    temp := int32(0)
    overflowCheck := int64(0)
    for input > 0 {
        temp = input % 10
        input /= 10
        overflowCheck = int64(result) * 10 + int64(temp)
        if overflowCheck > 2147483647 {
            return 0
        }
        result = int32(overflowCheck)
    }
    if isNegative {
        result *= -1
    }
    return int(result)
}