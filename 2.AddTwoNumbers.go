package main

import (
	"fmt"
)

func main() {
	fmt.Println("Hello, playground")
	l1 := new(ListNode)
	l1.Val = 2
	next := new(ListNode)
	l1.Next = next
	next.Val = 4
	next.Next = new(ListNode)
	next = next.Next
	next.Val = 3
	
	l2 := new(ListNode)
	l2.Val = 5
	next = new(ListNode)
	l2.Next = next
	next.Val = 6
	next.Next = new(ListNode)
	next = next.Next
	next.Val = 4
	
	res := addTwoNumbers(l1, l2)
	for res.Next != nil {
		println(res.Val)
		res = res.Next
	}
}

type ListNode struct {
      Val int
      Next *ListNode
}

func addTwoNumbers(l1 *ListNode, l2 *ListNode) *ListNode {
    result := new(ListNode)
    current := result
    carry := 0
    tempSum := 0
    for l1 != nil || l2 != nil || carry == 1 {
        tempSum += carry
        if l1 != nil {
            tempSum += l1.Val
            l1 = l1.Next
        }
        if l2 != nil {
            tempSum += l2.Val
            l2 = l2.Next
        }
        current.Val = tempSum % 10
        if tempSum > 9 {
            carry = 1
        } else {
            carry = 0
        }
	    tempSum = 0
        if l1 != nil || l2 != nil || carry == 1 {
            current.Next = new(ListNode)
            current = current.Next
        }
    }
    return result
}